package com.example.MyBookShopApp.data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "book")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "pub_date", nullable = false)
    private LocalDate publicationDate;

    @Column(nullable = false)
    private Boolean isBestseller;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String slug;

    private String image;

    @Column(columnDefinition = "TEXT")
    private String text;

    @Column(nullable = false)
    private Integer price;

    @Column(nullable = false, columnDefinition = "int default 0")
    private Integer discount;

    @OneToMany(mappedBy = "book")
    private List<BookToAuthor> authors;

    @OneToMany(mappedBy = "book")
    private List<BalanceTransaction> transactions;

    @OneToMany(mappedBy = "book")
    private List<Review> reviews;

    @ManyToMany
    @JoinTable(name = "book2genre")
    private List<Genre> genres;

    @OneToMany(mappedBy = "book")
    private List<FileDownload> fileDownloads;

    @OneToMany(mappedBy = "book")
    private List<BookToUser> users;

    public List<BookToUser> getUsers() {
        return users;
    }

    public void setUsers(List<BookToUser> users) {
        this.users = users;
    }

    public List<FileDownload> getFileDownloads() {
        return fileDownloads;
    }

    public void setFileDownloads(List<FileDownload> fileDownloads) {
        this.fileDownloads = fileDownloads;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public List<BalanceTransaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<BalanceTransaction> transactions) {
        this.transactions = transactions;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(LocalDate publicationDate) {
        this.publicationDate = publicationDate;
    }

    public Boolean getBestseller() {
        return isBestseller;
    }

    public void setBestseller(Boolean bestseller) {
        isBestseller = bestseller;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public List<BookToAuthor> getAuthors() {
        return authors;
    }

    public void setAuthors(List<BookToAuthor> authors) {
        this.authors = authors;
    }
}
