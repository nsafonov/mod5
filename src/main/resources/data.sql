insert into author (id, first_name, last_name, slug) values (1, 'Carlos', 'Dodshon', 'carlos-dodshon');

insert into book (id, pub_date, is_bestseller, title, slug, price)
values (1, NOW(), true, 'Exploit book', 'exploit-book', 256);

insert into book (id, pub_date, is_bestseller, title, slug, price)
values (2, NOW(), true, 'Exploit book 2', 'exploit-book', 256);

insert into book (id, pub_date, is_bestseller, title, slug, price)
values (3, NOW(), true, 'Exploit book 3', 'exploit-book', 256);

insert into book (id, pub_date, is_bestseller, title, slug, price)
values (4, NOW(), true, 'Exploit book', 'exploit-book', 256);

insert into book (id, pub_date, is_bestseller, title, slug, price)
values (5, NOW(), true, 'Exploit book 2', 'exploit-book', 256);

insert into book (id, pub_date, is_bestseller, title, slug, price)
values (6, NOW(), true, 'Exploit book 3', 'exploit-book', 256);

insert into book2author(sort_index, author_id, book_id) values (5, 1, 1);
insert into book2author(sort_index, author_id, book_id) values (5, 1, 2);
insert into book2author(sort_index, author_id, book_id) values (5, 1, 3);
insert into book2author(sort_index, author_id, book_id) values (5, 1, 4);
insert into book2author(sort_index, author_id, book_id) values (5, 1, 5);
insert into book2author(sort_index, author_id, book_id) values (5, 1, 6);